#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
int wartosc = 0;
void MainWindow::on_pushButton_clicked()
{
    wartosc++;
    ui->lineEdit->setText(QString::number(wartosc));
}


void MainWindow::on_pushButton_2_clicked()
{
    if(ui->lineEdit_2->text()=="Master" && ui->lineEdit_3->text()=="password"){
        ui->pushButton_2->setEnabled(false);
        ui->pushButton->setEnabled(true);
        ui->pushButton_3->setEnabled(true);

    }
}


void MainWindow::on_pushButton_3_clicked()
{
    ui->pushButton_2->setEnabled(true);
    ui->pushButton->setEnabled(false);
    ui->pushButton_3->setEnabled(false);
}

